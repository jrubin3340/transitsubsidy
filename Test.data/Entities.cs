﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;

// for data annotaions and validations
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSolution.data
{
    /*
  * Flash is used to display alerts to the screen
  */
    public class Flash
    {
        public string type { get; set; }
        public string body { get; set; }
        public string title { get; set; }
    }

    /*
     * Log entries are going to be kept in their own collection - Log (this may end if we really get into rabbit MQ)
     * We are also going to toss the log out to rabbit MQ - thus we need an app id and title (get from ARS)
     */
    public class Log : Entity
    {
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
        public string RefId { get; set; }
        public string AppId { get; set; }
        public string AppTitle { get; set; }
        public string URL { get; set; }
        //added some fields to map better to syslog
        //Severity: Emergency, Alert, Critical, Error, Warning, Notice, Informational, Debug - see http://en.wikipedia.org/wiki/Syslog or RFC5424
        public string Severity { get; set; }
        public string Hostname { get; set; }
        //App-Name - use AppTitle
    }

    /*
     * roles allow us to assign superusers that can work across locations
     */
    public class Role : Entity
    {
        public string Title { get; set; }
        public HashSet<string> Members { get; set; }
        public string Body { get; set; }
    }

    /*
     * MailAddresses are multipart.  Junkmail filters are currently catching things with display name - so we aren't using the DisplayName much
     * but we will store both parts for now (it is also needed for calendar entries)
     */
    public class eMailAddress
    {
        public string Address { get; set; }
        public string DisplayName { get; set; }
    }

    /*
     * A template is used for sending mail, or for providing text blurbs at various points
     * 
     * index:
     * Key
     */
    public class Template : Entity
    {
        public string Key { get; set; }
        public string Description { get; set; }
        public string Priority { get; set; }
        public eMailAddress From { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
        //retired, all templates will be html or mime
        //public bool isHTML { get; set; }
    }

}

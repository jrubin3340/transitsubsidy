﻿using System;
using System.Collections.Generic;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using MongoRepository;

// for data annotaions and validations
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace TestSolution.Data
{
    /*
 * Flash is used to display alerts to the screen
 */
    public class Flash
    {
        public string type { get; set; }
        public string body { get; set; }
        public string title { get; set; }
    }

    /*
     * Log entries are going to be kept in their own collection - Log (this may end if we really get into rabbit MQ)
     * We are also going to toss the log out to rabbit MQ - thus we need an app id and title (get from ARS)
     */
    public class Log : Entity
    {
        public string CreatedBy { get; set; }
        public DateTime Created { get; set; }
        public string Controller { get; set; }
        public string Action { get; set; }
        public string Comment { get; set; }
        public string RefId { get; set; }
        public string AppId { get; set; }
        public string AppTitle { get; set; }
        public string URL { get; set; }
        //added some fields to map better to syslog
        //Severity: Emergency, Alert, Critical, Error, Warning, Notice, Informational, Debug - see http://en.wikipedia.org/wiki/Syslog or RFC5424
        public string Severity { get; set; }
        public string Hostname { get; set; }
        //App-Name - use AppTitle
    }

    /*
     * roles allow us to assign superusers that can work across locations
     */
    public class Role : Entity
    {
        public string Title { get; set; }
        public HashSet<string> Members { get; set; }
        public string Body { get; set; }
    }

    /*
     * MailAddresses are multipart.  Junkmail filters are currently catching things with display name - so we aren't using the DisplayName much
     * but we will store both parts for now (it is also needed for calendar entries)
     */
    public class eMailAddress
    {
        public string Address { get; set; }
        public string DisplayName { get; set; }
    }

    /*
     * A template is used for sending mail, or for providing text blurbs at various points
     * 
     * index:
     * Key
     */
    public class Template : Entity
    {
        [Key]
        public string Key { get; set; }

        public string Description { get; set; }
        public string Priority { get; set; }
        public eMailAddress From { get; set; }
        public List<eMailAddress> SendTo { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }

       // public string PrivacyStmt { get; set; }
       // public string CoverLetter { get; set; }
       // public string CertificationStmt { get; set; }

    }

    /*
     * ADClass represents the data returned for a user from AD
     * maps to what is returned by 
     * https://x0202tnythnetpd.aa.ad.epa.gov/adservice/api/users/ADUsersList/all/region-r02 for the userlist
     * we don't have the feed from yue-on yet, so can't flesh this out
     * userlist is a list of names we are going to use in bloodhound, and has some information.  Once a name is selected, we go to the user feed for the remainder
     */
    public class ADClass
    {
        //from the userlist feed
        public string LanId { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string PersonnelType { get; set; }
        public string Affiliation { get; set; }
        public string UPN { get; set; }
        //from the user feed
        public string WorkForceID { get; set; }
        public string ManagerName { get; set; }
        //public ADManagerClass Manager { get; set; }
        public string CN { get; set; }
        //public string SeeAlso { get; set; }
        public string Company { get; set; }
        public string Organization { get; set; }
        public string DisplayName { get; set; }
        public string AmpBox { get; set; }
        public string ExpirationDate { get; set; }
    }

    /*
    * Employee record should be fed from outside feed; must update before creating new forms
    * index:
    * EmpId
    */
    public class Employee : Entity
    {


        [Key]
        public string LanId { get; set; }
        public string EmpId { get; set; }
        public string Email { get; set; }
        public string DisplayName { get; set; }
        public string Department { get; set; }
        public string FirstLineSupervisor { get; set; }
        public string Phone { get; set; }
 
    }

    /*
    * Cerification Form is created annually based on enrolled employees
    * index:
    * EmpId + YearOfCertification
     * 
     * Name, Department, and FirstLineSupervisor will come from Employee Lookup
    */
    public class Certification : Entity
    {
        [Required]
        public Employee EmpInfo { get; set; }

        [Required]
        public string YearOfCertiifcation { get; set; }

        public DateTime DateSigned { get; set; }
        public string EmpSignature { get; set; }
        public string SupervisorSignature { get; set; }
        public DateTime DateApproved { get; set; }
   
    }

 /*
     * Claim Form is created quarterly based on enrolled employees
 * index:
 * EmpId + QuarterEndingDate
  * 
  * Name, Department, and Phone will come from Employee Lookup
 *  QuarterEndingDate will come from dbProfile
 *  DueDate will come from dbProfile
 *  Actual months will be calculated based on the QuarterEndingDate
 */
    public class Claim : Entity
    {
        [Required]
        [Key]
        public Employee EmpInfo { get; set; }
        
        [Required]
        [Key]
        public DateTime QuarterEndingDate { get; set; }

        public DateTime DueDate { get; set; }
        public string Month1 { get; set; }
        public string Month2 { get; set; }
        public string Month3 { get; set; }
        public float Amount1 { get; set; }
        public float Amount2 { get; set; }
        public float Amount3 { get; set; }
        public float TotalAmount { get; set; }
        public string Remarks { get; set; }
        public string Status { get; set; }

    }

    /*
     * Questionnaire is created when an employee joins the program
 * index:
 * EmpId
  * 
  * Name, Department, Home Zip Code and Phone will come from Employee Lookup
     * Values for Rail, Bus, Ferry will come from CommuteOptions tables
 */
    public class Questionnaire : Entity
    {
        [Required]
        [Key]
        public Employee EmpInfo { get; set; }

        [Required]
        [Display(Name = "Date Submitted")]
        public DateTime DateSubmitted { get; set; }

        public string Zip { get; set; }

        [Display(Name = "Change in Commute?")]
        public string ChangeRoutine { get; set; }
        [Display(Name = "Daily Commute Cost")]
        public float CommuteCost { get; set; }
        [Display(Name = "Work Schedule")]
        public string WorkSchedule { get; set; }
        public List<string> Rail { get; set; }
        public List<string> Bus { get; set; }
        public List<string> Ferry { get; set; }
        public string Status { get; set; }
        public DateTime Updated { get; set; }
        public string UpdatedBy { get; set; }
    }

    /*
     * SystemInfo has the standard info that is needed for creating other forms
  * It will be updated every quarter by the Admin
  * 
  * 
 */
    public class ProfileInfo : Entity
    {
        public string Contact { get; set; }

        [Display(Name = "Maximum Claim Amount")]
        public string MaxMonthlyClaim { get; set; }
        [Display(Name = "Quarter Ending Date")]
        public string QuarterEndingDate { get; set; }
        [Display(Name = "Claim Due Date")]
        public string ClaimDueDate { get; set; }
        [Display(Name = "Is annual certification required?")]
        public string VerificationRequired { get; set; }
        public string Month1 { get; set; }
        public string Month2 { get; set; }
        public string Month3 { get; set; }
        [Range(20,28)]
        [Display(Name = "Number of WorkDays in FIRST month")]
        public int DaysPerMonth1 { get; set; }
        [Range(20,28)]
        [Display(Name = "Number of WorkDays in SECOND month")]
        public int DaysPerMonth2 { get; set; }
        [Range(20,28)]
        [Display(Name = "Number of WorkDays in THIRD month")]
        public int DaysPerMonth3 { get; set; }

        /* for upload to Compass */
        [Display(Name = "Bike Type")]
        public string BikeType { get; set; }
        [Display(Name = "Transit Type")]
        public string TransitType { get; set; }

        public string SFO { get; set; }
        public string ODBCName { get; set; }
        public string ConnectString { get; set; }
        public string ProcName { get; set; }

    }

    public class TransitOptions : Entity
        /*
         * Enter Bus, Rail, or Ferry as Type
         * Values for State
         * Enter all possibilities for names including Other
          */ 
    {
        [Required(ErrorMessage = "Type of Transit is required")]
        public string Type { get; set; }
        [Required(ErrorMessage = "State is required")]
        public string State { get; set; }
        [Required(ErrorMessage = "Company Name is required")]
        [Display(Name = "Company Name")]
        public string CompanyName { get; set; }

    }
}

﻿populateFromAD: function (data) {
    //we have been handed data from AD, now go and populate the form
    var _obj = this;

    require(["dojo/dom-attr"], function (domAttr) {
        //could have set all fields using 
        //document.getElementById("Client_ExpirationDate").value = data.expirationDate;
        //but as we are using dojo, do it the dojo way

        domAttr.set("Client_AD_LanId", "value", data.lanId);
        domAttr.set("Client_AD_FirstName", "value", data.firstName);
        domAttr.set("Client_AD_LastName", "value", data.lastName);
        domAttr.set("Client_AD_Phone", "value", data.phone);
        domAttr.set("Client_AD_Email", "value", data.email);
        domAttr.set("Client_AD_EmpID", "value", data.workForceID);
        domAttr.set("Client_AD_FirstLineSupervisor", "value", data.managerName);
        domAttr.set("Client_AD_Department", "value", data.organization);

        //now do the translations and fill in the stuff in client object

        //display name is first last
        domAttr.set("Client_DisplayName", "value", data.firstName + " " + data.lastName);

        //should this be switche to be firstName la/stName?
        var supervisorDisplayName = _widget.convertToDisplayName(data.managerName, ", ");
        domAttr.set("Client_Supervisor", "value", supervisorDisplayName);
        //domAttr.set("Client_Supervisor", "value", data.managerName);
        //document.getElementById("Client_Supervisor").value = data.managerName;

        domAttr.set("Client_LanId", "value", data.lanId);
        domAttr.set("Client_Organization", "value", data.organization);
        domAttr.set("Client_Phone", "value", data.phone);
        domAttr.set("Client_Email", "value", data.email);

        //this works because the field is set with a type of datetime
        //AD is not consistent - for some non-expiring users, the date is 9999-12-31T23:59:59.9999999, but for others it is null
        if (data.expirationDate == null ) {
            domAttr.set("Client_ExpirationDate", "value", "9999-12-31T23:59:59.9999999");
        } else {
            domAttr.set("Client_ExpirationDate", "value", data.expirationDate);
        }
    });
            
            
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using MongoRepository;
using TestSolution.Data;
using TestSolution.Rabbit;

namespace TestSolution.Controllers
{
    public class TransitOptionsController : BaseController
    {
        static MongoRepository<TransitOptions> transitoptionsrepo = new MongoRepository<TransitOptions>();
        //
        //
        // GET: /TransitOptions/
        public ActionResult Index()
        {
            return View(transitoptionsrepo);
        }

        //
        // GET: /TransitOptions/Details/5
        [AllowAnonymous]
        public ActionResult Details(string id)
        {
            TransitOptions TransitOptions = transitoptionsrepo.GetById(id);

            ViewBag.flash = TempData["flash"];

            bool isAdmin = isInRole(User.Identity.Name, "Admin");
            ViewBag.isAdmin = isAdmin;

            if (isAdmin)
            {
                //we need to set to ViewBag properties to have the delete button work
                ViewBag.id = id;
                //ViewBag.controller = "TransitOptions";
                ViewBag.controller = RouteData.Values["controller"].ToString();
            }

            return View(TransitOptions);
        }
        //
        //GET: /TransitOptions/Create
        public ActionResult Create()
        {
            return View(
                new TransitOptions() { }

            );
        }

        //
        // POST: /TransitOptions/Create
        // Would have prefered using [AllowHTML] on just the body, rather than disabling validation for this action
        // but as this action is an admin only action, we should be OK
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(TransitOptions TransitOptions)
        {
            try
            {

                transitoptionsrepo.Add(TransitOptions);

                //now log it
                bool logged = logAction("Created Transit Options for " + TransitOptions.Type, "Transit Option Created");

                TempData["flash"] = buildFlash("success", "TransitOptions Created", "TransitOptions has been created");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Creating TransitOptions");

                return View();
            }
        }


        //
        //GET: /TransitOptions/Edit/id
        public ActionResult Edit(string id)
        {
            TransitOptions TransitOptions = transitoptionsrepo.GetById(id);

            return View(TransitOptions);
        }

        //
        // POST: /TransitOptions/Edit
        // Would have prefered using [AllowHTML] on just the body, rather than disabling validation for this action
        // but as this action is an admin only action, we should be OK
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Edit(TransitOptions TransitOptions)
        {
            try
            {
                transitoptionsrepo.Update(TransitOptions);

                //now log it
                bool logged = logAction("Updated TransitOptions " + TransitOptions.Type, "Informational");

                TempData["flash"] = buildFlash("success", "TransitOptions Updated", "TransitOptions has been updated");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Updating TransitOptions");

                return View();
            }
        }


        //
        // POST: /TransitOptions/Delete
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                string comment = "";
                if (isInRole(User.Identity.Name, "Admin"))
                {
                    TransitOptions TransitOptions = transitoptionsrepo.GetById(id);
                    string title = TransitOptions.Type +  " - " + TransitOptions.CompanyName;

                    transitoptionsrepo.Delete(id);
                    //now log this          
                    comment = "The " + title + " Transit Option has been deleted";
                    logAction(comment, "Informational");

                    TempData["flash"] = buildFlash("success", "TransitOptions Deleted", comment);

                    return RedirectToAction("Index");
                }
                else
                {
                    comment = "You have insufficient rights to delete this TransitOptions";
                    logAction(comment, "Warning");
                    TempData["flash"] = buildFlash("warning", "Insufficient Rights", comment);

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                string comment = "Error deleting TransitOptions";
                logAction(comment, "Error");
                TempData["flash"] = buildFlash("danger", "Error", comment);

                return RedirectToAction("Index");

            }
        }

    }
}
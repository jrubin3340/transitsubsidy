﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace TestSolution.Controllers
{
    public class ProfileInfoController : Controller
    {
        //
        // GET: /ProfileInfo/
        public ActionResult Index()
        {
            return View();
        }

        //
        // GET: /ProfileInfo/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        //
        // GET: /ProfileInfo/Create
        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /ProfileInfo/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ProfileInfo/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        //
        // POST: /ProfileInfo/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        //
        // GET: /ProfileInfo/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        //
        // POST: /ProfileInfo/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}

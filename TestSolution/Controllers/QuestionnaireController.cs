﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using TestSolution.Data;
using TestSolution.Rabbit;
using System.Net.Mail;
using System.Text;

namespace TestSolution.Controllers
{
    public class QuestionnaireController : BaseController
    {
        static MongoRepository<Questionnaire> questionnairerepo = new MongoRepository<Questionnaire>();
        //supplies values for transit options
        static MongoRepository<TransitOptions> transitoptionsrepo = new MongoRepository<TransitOptions>();


         //
        // GET: /Questionnaire/Details/5
        [AllowAnonymous]
        public ActionResult Details(string id)
        {
            Questionnaire questionnaire = questionnairerepo.GetById(id);

            ViewBag.flash = TempData["flash"];

            bool isAdmin = isInRole(User.Identity.Name, "Admin");
            ViewBag.isAdmin = isAdmin;

            if (isAdmin)
            {
                //we need to set to ViewBag properties to have the delete button work
                ViewBag.id = id;
                //ViewBag.controller = "Template";
                ViewBag.controller = RouteData.Values["controller"].ToString();
            }

            return View(questionnaire);
        }



        // will return the questionnaire for current user, or else return null
        public Questionnaire getMyQuestionnaire()
        {
            try
            {
                string user = User.Identity.Name.ToLower();
                //we want to correct for user names in the form AA\tpmurphy
                if (user.Contains("\\"))
                {
                    user = user.Substring(3);
                }

                Questionnaire questionnaire = questionnairerepo.First(c => c.EmpInfo.LanId.ToLower() == user && (c.Status == "Approved" || c.Status == "Request"));

                return questionnaire;
            }
            catch
            {
                return null;
            }
        }

        // will return the questionnaire for current user, or else return null
        public TransitOptions getOptions()
        {
            try
            {
               TransitOptions railoptions  = transitoptionsrepo.First(c => c.Type == "Rail");

                return railoptions;
            }
            catch
            {
                return null;
            }
        }

        //
        // GET: /Questionnaire/JSON
        //throw questionnaire back as json
        //used for ticket finder typeahead
        [AllowAnonymous]
        public JsonResult JSON(string id)
        {
            return Json(questionnairerepo, JsonRequestBehavior.AllowGet);
        }

        //
        // GET: /Ticket/
        // GET: /Ticket/Index
        // GET: /Ticket/Index/NY
        // Going to return all passes.  If a location is specified, we filter it to that
        [AllowAnonymous]
        public ActionResult Index(string id)
        {
            ViewBag.flash = TempData["flash"];
            ViewBag.isAdmin = isInRole(User.Identity.Name, "Admin");
            ViewBag.location = id;

            //as we want to pass either the index, or a filtered index, 
            //the model is defined as @model MongoDB.Driver.Linq.MongoQueryable<parkingPass.Data.Pass>
            //rather than the more typical @model MongoRepository.MongoRepository<parkingPass.Data.Pass>
            System.Linq.IOrderedQueryable<TestSolution.Data.Questionnaire> result = null;
            if (id == null)
            {
                result = questionnairerepo.OrderBy(s => s.EmpInfo.DisplayName);
            }
            else
            {
                result = questionnairerepo.Where(c => c.EmpInfo.LanId == id).OrderBy(s => s.EmpInfo.DisplayName);
            }

            return View("Display", result);
            //return View(passrepo);
        }

        public bool isMyQuestionnaire(Questionnaire questionnaire)
        {
            string user = User.Identity.Name;
            string owner = "";
            if (questionnaire.EmpInfo.LanId != null)
            {
                owner = questionnaire.EmpInfo.LanId.ToLower();
            }

            //we want to correct for user names in the form AA\tpmurphy
            if (user.Contains("\\"))
            {
                user = user.Substring(3).ToLower();
            }

            return (user == owner);
        }

        //
        // GET: /Questionnaire/MyQuestionnaire
        // will return the current users questionnaire, if it exists, otherwise will bounce them to a "create a new questionnaire" page\
        // or maybe just send them to the welcome page with a flash that explains all
        public ActionResult MyQuestionnaire()
        {

            Questionnaire questionnaire = getMyQuestionnaire();

            if (questionnaire == null)
            {
                string comment = "No Questionnaire for you has been found - please create one to join the Public Transit Subsidy program";
                TempData["flash"] = buildFlash("warning", "No Questionnaire Found", comment);
                //ViewBag.flash = TempData["flash"];

                string user = User.Identity.Name.ToLower();
                //we want to correct for user names in the form AA\tpmurphy
                if (user.Contains("\\"))
                {
                    user = user.Substring(3);
                }
                ViewBag.lanid = user;

                ViewBag.isAdmin = isInRole(User.Identity.Name, "Admin");

                return View("Create",
                    new Questionnaire(
                    //insert value for Date Submitted
                     )
                           );
            }
            else
            {
               // bool isAdmin = isInRole(User.Identity.Name, "Admin");
               // ViewBag.isAdmin = isAdmin;
               // ViewBag.isMyQuestionnaire = isMyQuestionnaire(questionnaire);

                //we need to set to ViewBag properties to have the delete button, add plate, and remove plate buttons work
                ViewBag.id = questionnaire.Id;
                ViewBag.controller = RouteData.Values["controller"].ToString();

                return View("Create", questionnaire);
            }

        }
        
        //
        // POST: /Questionnaire/MyQuestionnaire
        [HttpPost]
        public ActionResult MyQuestionnaire(Questionnaire questionnaire)
        {
            try
            {
                
                string user = User.Identity.Name.ToLower();
                   //we want to correct for user names in the form AA\tpmurphy
                if (user.Contains("\\"))
                  {
                      user = user.Substring(3);
                  }
 
                if (ModelState.IsValid)
                {
 
                  if (questionnaire.Status != null) {

                        if (questionnaire.Status == "Accepted")
                        {

                            //get old questionnaire because the current one is changed
                            //and change status to archive to save old form

                            Questionnaire questionnaireArchive = questionnairerepo.First(c => c.EmpInfo.LanId == user && c.Status == "Request");  //change when get something to be approved

                            questionnaireArchive.Status = "Archive";
                            questionnaireArchive.Updated = DateTime.Now;
                            questionnaireArchive.UpdatedBy = user;
                            questionnairerepo.Update(questionnaireArchive);
                        }
                        else
                        {
                            questionnaire.Updated = DateTime.Now;
                            questionnaire.UpdatedBy = user;
                            questionnaire.DateSubmitted = DateTime.Now;
                            questionnairerepo.Update(questionnaire);
                        }


                    }
                    else {        //new questionnaire
                    
                    //store new updated questionnaire to be approved
                    questionnaire.Updated = DateTime.Now;
                    questionnaire.UpdatedBy = user;
                    questionnaire.DateSubmitted = DateTime.Now;

                    questionnaire.Status = "Request";  //keep this for archive, then new one
                    questionnaire.UpdatedBy = user;

                    questionnairerepo.Add(questionnaire);
                    }

                    string comment = User.Identity.Name + " has created or modified a questionnaire.  Questionnaire id: " + questionnaire.Id;
                    string alertType = "success";

                    
                    //right here we need to send the message to the admin for approval of the questionnaire

                    if (sendTemplate("Questionnaire_Create", questionnaire))
                    {
                        comment = comment + ".  Email message sent.";
                    }
                    else
                    {
                        comment = comment + ".  Email message NOT sent.";
                        alertType = "warning";
                    }


                    //now log it
                    bool logged = logAction(comment, "Informational");

                    TempData["flash"] = buildFlash(alertType, "Questionnaire Created", comment);

                    //return RedirectToAction("Display", "Template", new { id = "Post_SelfCreate" });
                   return RedirectToAction("Index", "Questionnaire");
                }
                else  //failed validation
                {
                    return View(questionnaire);
                }

            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Creating Questionnaire - ModelState: " + ModelState.IsValid);

                return View();
            }
        }

        //
        // GET: /Questionnaire/Edit/5
        public ActionResult Edit(string id)
        {
            Questionnaire questionnaire = questionnairerepo.GetById(id);

            return View(questionnaire);
        }
        //
        // POST: /Questionnaire/Edit/5
        [HttpPost]
        public ActionResult Edit(Questionnaire questionnaire)

        {
            try
            {
                questionnairerepo.Update(questionnaire);

                //now log it
                bool logged = logAction("Updated Questionnaire " + questionnaire.EmpInfo.EmpId, "Informational");

                TempData["flash"] = buildFlash("success", "Questionnaire Updated", "Questionnaire has been updated");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Updating Questionnaire");

                return View();
            }
        }


        //
        // POST: /Questionnaire/Delete/5
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                string comment = "";
                if (isInRole(User.Identity.Name, "Admin"))
                {
                    Questionnaire questionnaire = questionnairerepo.GetById(id);
                    string title = questionnaire.EmpInfo.DisplayName;

                    questionnairerepo.Delete(id);
                    //now log this          
                    comment = "The questionnaire for " + title + " has been deleted";
                    logAction(comment, "Informational");

                    TempData["flash"] = buildFlash("success", "Questionnaire Deleted", comment);

                    return RedirectToAction("Display");
                }
                else
                {
                    comment = "You have insufficient rights to delete this template";
                    logAction(comment, "Warning");
                    TempData["flash"] = buildFlash("warning", "Insufficient Rights", comment);

                    return RedirectToAction("Display");
                }
            }
            catch
            {
                string comment = "Error deleting questionnaire";
                logAction(comment, "Error");
                TempData["flash"] = buildFlash("danger", "Error", comment);

                return RedirectToAction("Display");

            }
        }

       //
        // we need to notify - send template
        public bool sendTemplate(string root_templatename, Questionnaire questionnaire)
        {
            try
            {
                //try to send message
                MailAddressCollection toCollection = new MailAddressCollection();
                MailAddressCollection ccCollection = new MailAddressCollection();
                string templatename = root_templatename;

                switch (root_templatename)
                {
                    case "Questionnaire_Create":
                    case "Notification_TagIssued":
                    {
                        //templatename = root_templatename + "_" + questionnaire.EmpInfo.Department;
                        templatename = root_templatename;
                        break;
                    }
                }
                
                MongoRepository<Template> templaterepo = new MongoRepository<Template>();
                Template template = templaterepo.First(c => c.Key == templatename);


                //some templates may have different sendto and from rules
                string fromstr = "";
                switch (root_templatename)
                {
                    case "Questionnaire_Create": 
                    {
                        fromstr = questionnaire.EmpInfo.LanId;

                        //for testing only
                        toCollection.Add(new MailAddress("rubin.judy@epa.gov"));

                        break;
                    }
                    /*case "Notification_TagIssued": 
                    {
                        fromstr = template.From.Address;

                        toCollection.Add(new MailAddress(ticket.Client.Email));

                        //for testing only
                        //toCollection.Add(new MailAddress("murphy.tom@epa.gov"));

                        break;
                    }*/

                }

                //public bool smtpMailHelper(MailAddressCollection toCollection, MailAddressCollection ccCollection, string subject, string body, bool isHtml, MailAddress from, string priority)
                string subject = decodedTemplate(questionnaire, template.Subject);
                string body = decodedTemplate(questionnaire, template.Body);

                return smtpMailHelper(toCollection, ccCollection, subject, body, true, new MailAddress(fromstr), "Normal");
                //return true;
                
            }
            catch
            {
                return false;
            }
        }

        //given a string, will replace any of our replacement sring with the appropriate value from our ticket
        //we use stringbuilder to avoid the performance problems of normal replaces (which builds a new string each time)
        private string decodedTemplate(Questionnaire questionnaire, string templatestring)
        {
            try
            {
                //{{client}}, {{db_url}}, {{db_link}}, {{doc_url}}, {{doc_link}}
                StringBuilder tstr = new StringBuilder(templatestring);
                string dburl = buildURL(null, null, null);
                tstr.Replace("{{db_url}}", dburl);

                //{{db_link}}
                string dblink = "<a href=\"" + dburl + "\">Public Transit Subsidy Program" + "</a>";
                tstr.Replace("{{db_link}}", dblink);

                //{{client}}
                tstr.Replace("{{client}}", questionnaire.EmpInfo.DisplayName);

                //{{comment}}
                //tstr.Replace("{{comment}}", invite.Comment);

                //{{doc_url}}
                string docurl = buildURL("Questionnaire", "Details", questionnaire.Id);
                tstr.Replace("{{db_url}}", dburl);

                //{{doc_link}}
                string doclink = "<a href=\"" + dburl + "\">Questionnaire for " + questionnaire.EmpInfo.DisplayName + "</a>";
                tstr.Replace("{{doc_link}}", doclink);

                return tstr.ToString();
            }
            catch
            {
                return templatestring;
            }
        }


/*
        // GET: /Questionnaire/SelfCreate
        // the user is creating a questionnaire for themselves
        public ActionResult Create()
        {

            //ViewData["Office"] = new SelectList(people, "UserID", "UserName");
            //ViewBag.primaryPlate = "";

            string user = User.Identity.Name.ToLower();
            //we want to correct for user names in the form AA\tpmurphy
            if (user.Contains("\\"))
            {
                user = user.Substring(3);
            }
            ViewBag.lanid = user;

            ViewBag.isAdmin = isInRole(User.Identity.Name, "Admin");

            return View(
                new Questionnaire(
                    //insert value for Date Submitted
                    )
                {
                }
            );
        }
        */
    }
}

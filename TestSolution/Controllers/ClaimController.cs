﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using TestSolution.Data;
using TestSolution.Rabbit;

namespace TestSolution.Controllers
{
    public class ClaimController : BaseController
    {
        static MongoRepository<Claim> claimrepo = new MongoRepository<Claim>();

        // will return the claim for current user, or else return null
        public Claim getMyClaim()
        {
            try
            {
                string user = User.Identity.Name.ToLower();
                //we want to correct for user names in the form AA\tpmurphy
                if (user.Contains("\\"))
                {
                    user = user.Substring(3);
                }

                Claim claim = claimrepo.First(c => c.EmpInfo.LanId.ToLower() == user);

                return claim;
            }
            catch
            {
                return null;
            }
        }
        
        //
        // GET: /Claim/
        public ActionResult Index()
        {
            return View(claimrepo);
        }

        //
        // GET: /Claim/Details/5
        [AllowAnonymous]
        public ActionResult Details(string id)
        {
            Claim claim = claimrepo.GetById(id);

            ViewBag.flash = TempData["flash"];

            bool isAdmin = isInRole(User.Identity.Name, "Admin");
            ViewBag.isAdmin = isAdmin;

            if (isAdmin)
            {
                //we need to set to ViewBag properties to have the delete button work
                ViewBag.id = id;
                //ViewBag.controller = "Template";
                ViewBag.controller = RouteData.Values["controller"].ToString();
            }

            return View(claim);
        }
        //
        //GET: /Claim/Create
        public ActionResult Create()
        {
            return View(
                new Claim() { }

            );
        }

        //
        // POST: /Claim/Create
        // Would have prefered using [AllowHTML] on just the body, rather than disabling validation for this action
        // but as this action is an admin only action, we should be OK
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(Claim claim)
        {
            try
            {

                claimrepo.Add(claim);

                //now log it
                bool logged = logAction("Created Claim " + claim.EmpInfo.EmpId, "Informational");

                TempData["flash"] = buildFlash("success", "Claim Created", "Claim has been created");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Creating Claim");

                return View();
            }
        }



        //
        // GET: /Claim/Edit/5
        public ActionResult Edit(string id)
        {
            Claim claim = claimrepo.GetById(id);

            return View(claim);
        }
        //
        // POST: /Claim/Edit/5
        [HttpPost]
        public ActionResult Edit(Claim claim)
        {
            try
            {
                claimrepo.Update(claim);

                //now log it
                bool logged = logAction("Updated Claim " + claim.EmpInfo.EmpId, "Informational");

                TempData["flash"] = buildFlash("success", "Claim Updated", "Claim has been updated");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Updating Claim");

                return View();
            }
        }


        //
        // POST: /Claim/Delete/5
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                string comment = "";
                if (isInRole(User.Identity.Name, "Admin"))
                {
                    Claim claim = claimrepo.GetById(id);
                    string title = claim.EmpInfo.DisplayName;

                    claimrepo.Delete(id);
                    //now log this          
                    comment = "The claim for " + title + " has been deleted";
                    logAction(comment, "Informational");

                    TempData["flash"] = buildFlash("success", "Claim Deleted", comment);

                    return RedirectToAction("Index");
                }
                else
                {
                    comment = "You have insufficient rights to delete this template";
                    logAction(comment, "Warning");
                    TempData["flash"] = buildFlash("warning", "Insufficient Rights", comment);

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                string comment = "Error deleting claim";
                logAction(comment, "Error");
                TempData["flash"] = buildFlash("danger", "Error", comment);

                return RedirectToAction("Index");

            }
        }
    }
}


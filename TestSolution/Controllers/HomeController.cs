﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using TestSolution.Data;
using TestSolution.Rabbit;

namespace TestSolution.Controllers
{
    public class HomeController : BaseController
    {
        static MongoRepository<Template> templaterepo = new MongoRepository<Template>();

        public ActionResult Index()
        {
            MongoRepository<Template> templaterepo = new MongoRepository<Template>();
            //how do we test if we have a connection to the database
            if (1 == 2)
            {
                //return RedirectToAction("DatabaseNotFound");
            }
            else
            {
                Template template = null;
                string key = "Welcome";

                ViewBag.isAdmin = isInRole(User.Identity.Name, "Admin");


                if (templaterepo.Exists(c => c.Key == key))
                {
                    template = templaterepo.First(c => c.Key == key);
                }
                else
                {
                    template = new Template()
                    {
                        Subject = "Welcome",
                        Body = "A custom Welcome page has not been created for this <strong>application</strong> - you can create one under Admin - Templates"
                    };
                }
                return View(template);
            }
        }

        public ActionResult About()
        {
            ViewBag.Message = "Public Transit Subsidy Program description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "For more information, please contact Steven Braunstein";

            return View();
        }
    }
}
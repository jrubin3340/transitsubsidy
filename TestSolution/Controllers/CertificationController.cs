﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MongoRepository;
using TestSolution.Data;
using TestSolution.Rabbit;

namespace TestSolution.Controllers
{
    public class CertificationController : BaseController
    {
        static MongoRepository<Certification> certificationrepo = new MongoRepository<Certification>();

        // will return the certification for current user, or else return null
        public Certification getMyCertification()
        {
            try
            {
                string user = User.Identity.Name.ToLower();
                //we want to correct for user names in the form AA\tpmurphy
                if (user.Contains("\\"))
                {
                    user = user.Substring(3);
                }

                Certification certification = certificationrepo.First(c => c.EmpInfo.LanId.ToLower() == user);

                return certification;
            }
            catch
            {
                return null;
            }
        }

        //
        // GET: /Certification/
        public ActionResult Index()
        {
            return View(certificationrepo);
        }

        //
        // GET: /Certification/Details/5
        [AllowAnonymous]
        public ActionResult Details(string id)
        {
            Certification certification = certificationrepo.GetById(id);

            ViewBag.flash = TempData["flash"];

            bool isAdmin = isInRole(User.Identity.Name, "Admin");
            ViewBag.isAdmin = isAdmin;

            if (isAdmin)
            {
                //we need to set to ViewBag properties to have the delete button work
                ViewBag.id = id;
                //ViewBag.controller = "Template";
                ViewBag.controller = RouteData.Values["controller"].ToString();
            }

            return View(certification);
        }
        //
        //GET: /Certification/Create
        public ActionResult Create()
        {
            return View(
                new Certification() { }

            );
        }

        //
        // POST: /Certification/Create
        // Would have prefered using [AllowHTML] on just the body, rather than disabling validation for this action
        // but as this action is an admin only action, we should be OK
        [ValidateInput(false)]
        [HttpPost]
        public ActionResult Create(Certification certification)
        {
            try
            {

                certificationrepo.Add(certification);

                //now log it
                bool logged = logAction("Created certification " + certification.EmpInfo.EmpId, "Informational");

                TempData["flash"] = buildFlash("success", "Certification Created", "Certification has been created");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Creating Certification");

                return View();
            }
        }



        //
        // GET: /Certification/Edit/5
        public ActionResult Edit(string id)
        {
            Certification certification = certificationrepo.GetById(id);

            return View(certification);
        }
        //
        // POST: /Certification/Edit/5
        [HttpPost]
        public ActionResult Edit(Certification certification)
        {
            try
            {
                certificationrepo.Update(certification);

                //now log it
                bool logged = logAction("Updated Certification " + certification.EmpInfo.EmpId, "Informational");

                TempData["flash"] = buildFlash("success", "Certification Updated", "Certification has been updated");

                return RedirectToAction("Index");
            }
            catch
            {
                ViewBag.flash = buildFlash("danger", "Error", "Error Updating Certification");

                return View();
            }
        }


        //
        // POST: /Certification/Delete/5
        [HttpPost]
        public ActionResult Delete(string id)
        {
            try
            {
                string comment = "";
                if (isInRole(User.Identity.Name, "Admin"))
                {
                    Certification certification = certificationrepo.GetById(id);
                    string title = certification.EmpInfo.DisplayName;

                    certificationrepo.Delete(id);
                    //now log this          
                    comment = "The Certification for " + title + " has been deleted";
                    logAction(comment, "Informational");

                    TempData["flash"] = buildFlash("success", "Certification Deleted", comment);

                    return RedirectToAction("Index");
                }
                else
                {
                    comment = "You have insufficient rights to delete this Certification";
                    logAction(comment, "Warning");
                    TempData["flash"] = buildFlash("warning", "Insufficient Rights", comment);

                    return RedirectToAction("Index");
                }
            }
            catch
            {
                string comment = "Error deleting Certification";
                logAction(comment, "Error");
                TempData["flash"] = buildFlash("danger", "Error", comment);

                return RedirectToAction("Index");

            }
        }
    }
}
